<?php
/*
Plugin Name:  Portfolio Project Post Type
Description:  Create portfolio projects for Wordpress.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

namespace WendyRowe\PortfolioProjectPostType;

function create_portfolio_project_post_type() {
	register_post_type('portfolio_project',
		array(
			'labels' => array(
				'name' => __('Portfolio'),
				'singular_name' => __('Project'),
				'menu_name' => __('Portfolio'),
				'all_items' => __('All Projects'),
				'add_new_item' => __('Add New Project'),
				'edit_item' => __('Edit Project'),
				'new_item' => __('New Project'),
				'view_item' => __('View Project'),
				'search_items' => __('Search Projects'),
				'not_found' => __('No projects found'),
				'not_found_in_trash' => __('No projects found in Trash'),
			),
			'public' => true,
			'exclude_from_search' => true,
			'has_archive' => true,
			'menu_position' => 20,
			'menu_icon'   => 'dashicons-portfolio',
			'supports' => array(
				'title',
				'thumbnail',
				'page-attributes'
			),
			'rewrite' => array(
				'slug' => 'portfolio'
			)
		)
	);
}
add_action('init', __NAMESPACE__ . '\\create_portfolio_project_post_type');

function query_vars($qvars) {
	$qvars[] = 'selected_portfolio_project';
	return $qvars;
}
add_filter('query_vars', __NAMESPACE__ . '\\query_vars');

function add_rewrite_rules() {
	add_rewrite_rule(
		'^portfolio\/([^/]*)(\/page\/([0-9]+))?$',
		'index.php?post_type=portfolio_project&selected_portfolio_project=$matches[1]&paged=$matches[3]',
		'top'
	);
}
add_action('init', __NAMESPACE__ . '\\add_rewrite_rules');

function pre_get_posts($query) {
	if (is_admin() || !$query->is_main_query())
		return;

	if (is_post_type_archive('portfolio_project')) {
		$selected_query_var = get_query_var('selected_portfolio_project');
		if(!empty($selected_query_var)) {
			$selected_post = new \WP_Query(array(
				'post_type' => 'portfolio_project',
				'post_status' => 'publish',
				'posts_per_page' => 1,
				'name' => $selected_query_var,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			));
			if (!$selected_post->found_posts) {
				$query->set_404();
				status_header(404);
			}
		}
		$query->set('posts_per_page', 60);
		return;
	}
}
add_action('pre_get_posts', __NAMESPACE__ . '\\pre_get_posts');

if (!class_exists('PortfolioProjectFeaturedImageColumn')) {

	class PortfolioProjectFeaturedImageColumn {

		const domain  = 'portfolio-project-featured-image-column';
		const version = '0.1';

		/**
		 * Ensures that the rest of the code only runs on edit.php pages
		 */
		function __construct() {
			add_action('load-edit.php', array($this, 'load'));
		}

		/**
		 * Since the load-edit.php hook is too early for checking the post type, hook the rest
		 * of the code to the wp action to allow the query to be run first
		 */
		function load() {
			add_action('wp', array($this, 'init'));
		}

		function init() {

			$post_type = get_post_type();

			if ($post_type !== 'portfolio_project')
				return;

			/* Print style */
			add_action('admin_enqueue_scripts', array($this, 'style'), 0);

			/* Column manager */
			add_filter("manage_edit-{$post_type}_columns", array($this, 'columns'));
		//	add_filter("manage_{$post_type}_posts_columns",		array($this, 'columns'));
			add_action("manage_{$post_type}_posts_custom_column", array($this, 'column_data'), 10, 2);
		}

		/**
		 * Enqueue stylesheaet
		 */
		function style() {
			wp_register_style('portfolio-project-featured-image-column', plugin_dir_url(__FILE__) . 'css/column.css', null, self::version);
			wp_enqueue_style('portfolio-project-featured-image-column');
		}

		/**
		 * Filter the image in before the 'title'
		 */
		function columns($columns) {

			if (!is_array($columns))
				$columns = array();

			$new = array();

			foreach($columns as $key => $title) {
				if ($key == 'title') // Put the Thumbnail column before the Title column
					$new['featured-image'] = __('Image', self::domain);

				$new[$key] = $title;
			}

			return $new;
		}

		/**
		 * Output the image
		 */
		function column_data($column_name, $post_id) {

			if ('featured-image' != $column_name)
				return;

			$image_src = self::get_the_image($post_id);

			if (empty($image_src)) {
				echo "&nbsp;"; // This helps prevent issues with empty cells
				return;
			}

			echo '<img alt="' . esc_attr(get_the_title()) . '" src="' . esc_url($image_src) . '" />';
		}

		/**
		 * Function to get the image
		 */
		function get_the_image($post_id = false) {

			$post_id	= (int) $post_id;
			$cache_key	= "featured_image_post_id-{$post_id}-_thumbnail";
			$cache		= wp_cache_get($cache_key, null);

			if (!is_array($cache))
				$cache = array();

			if (!array_key_exists($cache_key, $cache)) {
				if (empty($cache) || !is_string($cache)) {
					$output = '';

					if (has_post_thumbnail($post_id)) {
						$image_array = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), array(36, 32));

						if (is_array($image_array) && is_string($image_array[0]))
							$output = $image_array[0];
					}

					if (empty($output)) {
						$output = plugins_url('images/default.png', __FILE__);
					}

					$output = esc_url($output);
					$cache[$cache_key] = $output;

					wp_cache_set($cache_key, $cache, null, 60 * 60 * 24 /* 24 hours */);
				}
			}

			// Make sure we're returning the cached image HT: https://wordpress.org/support/topic/do-not-display-image-from-cache?replies=1#post-6773703
			return isset($cache[$cache_key]) ? $cache[$cache_key] : $output;
		}
	}
	$portfolio_project_featured_image_column = new PortfolioProjectFeaturedImageColumn;
};